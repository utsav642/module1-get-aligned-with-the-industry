# CNN Summary


**'Convolutional Neural Network'** is mainly used for the classification.
i.e. after studying the image we can predict wheather there is cat or dog in  
the provided input image.


![CNN layers](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/8280d7f007a9f7d07eae922ea7102477/dnn2.png)
credits : GitHub - IoTIoT - Project Internship - Module 1 - Get alligned with the industry - Wiki - CNN 
source : [GitHub link](https://gitlab.com/iotiotdotin/project-internship-ai/module1-get-aligned-with-the-industry/-/wikis/CNN)

here, we talk about filters i.e. matrix which has a particular feature for example  
dog's eye, cat's nose, etc.

We use sliding window algorithm in this, to go through all the possible features of the image.

As we can see in figure we have few convolutional layers in between the input  
image and the start of 'DNN'

we see kernel valid padding, max pooling, Flattened layers here obviously each one having  
a distinct function to do...

at last, we see the ReLU function. it is the most popular function used after convolutional  
layers to get the activation of neurons...

** the neurons we use in the DNN are actually comes from this ReLU function! **


