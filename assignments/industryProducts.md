## Product 1

#### Product name
Estimating 3D Poses of Athletes at Live Sporting Events.  

#### Product link
[link](https://news.developer.nvidia.com/nyt-wrnch-ai-3d-pose-estimation/)  

#### Product Short description
To improve how sporting events are covered in the news...   
A new AI 3D pose estimation model.  

#### Product is combination of features
 * TensorRT  
 * Pose Estimation  
 * Video Processing  

#### Product is provided by which company?
**Wrnch**, an AI-computer vision company (and NVIDIA Inception program.)            


## Product 2

#### Product name
making media content shoppable.  

#### Product link
[link](https://news.developer.nvidia.com/inception-spotlight-startup-uses-ai-to-identify-products-in-videos/)  

#### Product Short description
unveiled a new deep learning-based algorithm that can automatically decode what a celebrity, athlete,   
or other public figure is wearing in a video in near real time.  

#### Product is combination of features
 * Image Recognition
 * Neural Networks
 * TensorFlow  
 

#### Product is provided by which company?
New York City-based startup **TheTake**      


