# DNN Summary

**DNN** stands for 'Deep Neural Network'.
In the flow, first we'll see what is neural network, and then
the concepts related to 'DNN'  


##### What is Neural Network?

as the word itself suggests, it is a network of neurons...  
but what are the neurons referring to here? yes we've heard  
in the study of human brain and nervous system but obviously it
is not the reference here.

So, neuron here means a thing that holds a number between 0 and 1.
and due to the fact that we are using it as a carriers of (kind of)
signals for the AI system(program) to get intelligence we refer it as  
**neurons**. as in human body also the message carriers are called as  
the same.

technically, neurons are the output of the "ReLU Activation function" we saw  
in CNN...  

number inside the neuron is called "activation".  
and these numbers actually represent the gray scale measurement...
value = 0   ---->    black pixel
0 < value < 1    ---->   intermediary colours 
value = 1   ---->    white pixel


therefore, when I say 28*28 pixel image, there are 28 rows and 28  
columns of neurons.

![28*28 pixel image](https://www.ovh.com/blog/wp-content/uploads/2020/06/1_cLsTCWtUL1GYBUv8vnbOxw.jpeg)
credits: "Deep Learning explained to my 8-year-old daughter" By Jean-Louis Queguiner 
source: [above mentioned blog](https://www.ovh.com/blog/deep-learning-explained-to-my-8-year-old-daughter/)
 
by the way it is represented in rgb therefore have values in the 0-255 range.
to depict different colours i.e. red,yellow,etc. whereas,
in grayscale scenario, we only need to show two colours i.e. white and black.
that's the difference.  

picture above is just to give you the idea how image forms and how we get the  
input image in the form of neurons to analayse it.  

##### Deep Neural Network

Now, after taking input image, we want to analayse the patterns in it.  

![neural network](https://nextjournal.com/data/1220CC01595BBCB08CCAC75AC0A373519699CFBC6FB7E6118A92DDB89EDB63490CFE?filename=text4298.png&content-type=image/png)
credits : MNIST Handwritten Digit Recognition in Keras by 'Gregor Koehler'
source : [above mentioned article link](https://nextjournal.com/gkoehler/digit-recognition-with-keras) 

Taking into consideration that, we want to recognize the handwritten digits(0-9)  
i.e. the "hello world" of neural networks...  

Above image explains how it gets done!  

basically among the two hidden layers(middle) in the above neural network image,  
for an instance, we can assume that first layer recognizes the edges in the image  
the second layer may have been recognizing the patterns in the image i.e. loops  
and lines and then last layer which have 10 neurons in it representing the 10 digits  
give the final output.


that is,
7th neuron in the last layer is brightest  --->   digit recognized = 7

obviously, this training of the system involves so much complex mathematics  
especially, the calculus and matrices...


We come across two functions in this,
 1. Neural Network Function
 2. Cost Function
 
##### Neural Network Function

**in case of 28*28 pixel image,**

**input : 784(28*28) numbers(pixels)**
**parameters : 13,002 total weights and biases**   
**output : 10 numbers**  

here, weights and biases are the values we take to derive the required following  
layers in the network.

Actually, activation values for the neurons in the layers following the first layer  
are derived from the same weights and biases values.

a = sigmoid(w1a1 + w2a2 + ... + w784a784 + bias)
here, w1,w2 are weights
a1,a2 are activation values of previous layer neurons
bias is bias...
sigmoid is a function

![sigmoid function](https://dvqlxo2m2q99q.cloudfront.net/000_clients/981864/file/9818648EisiqmG.png)
credits : The neuron Activator blog
source : [link to the above blog](https://www.neuronactivator.com/blog/what-even-is-activation-function)


##### Cost function and BackPropagation

**input : 13,002 weights and biases**
**output : 1 number(the cost)**
**parameters : many many many training examples**

this is basically deviation from the correct result.
we are calculating the cost of our deviation. 
i.e. how much we have deviated from the actual correct answer

so, we should try to minimize this cost so that our prediction will  
get more accurate.

and **this is exactly what the BackPropagation tells us...**
it is the concept to back propafate through layers and inspect for the  
possible bugs in the logic due to which we are getting more cost. and how  
can we minimize it...


#### Thank You!



